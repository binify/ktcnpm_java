package weightedscoringmodel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import financeanalysis.InputData;

public class MainClass {

	String[] tieuchi;
	int[] trongso;
	int[][] diem;
	double score;

	public void readData(String filePath, String fileName, String sheetName) throws IOException {

		File file = new File(filePath + "\\" + fileName);
		FileInputStream inputStream = new FileInputStream(file);
		Workbook workBook = null;

		String fileExtension = fileName.substring(fileName.indexOf("."));

		if (fileExtension.equals(".xlsx")) {
			workBook = new XSSFWorkbook(inputStream);
		} else if (fileExtension.equals(".xls")) {
			workBook = new HSSFWorkbook(inputStream);
		}

		Sheet sheet = workBook.getSheet(sheetName);

		int lastRow = sheet.getLastRowNum();
		int lastCol = sheet.getRow(0).getLastCellNum();
		tieuchi = new String[lastRow];
		trongso = new int[lastRow];
		diem = new int[lastRow][lastCol];

		Row row;
		for (int i = 1; i <= lastRow; i++) {
			row = sheet.getRow(i);
			tieuchi[i - 1] = row.getCell(0).getStringCellValue();
			trongso[i - 1] = (int) row.getCell(1).getNumericCellValue();
			for (int j = 2; j < lastCol; j++) {
				diem[i - 1][j - 2] = (int) row.getCell(j).getNumericCellValue();
			}
		}
	}

	public void printData() {
		for (int i = 1; i <= trongso.length; i++) {
			for (int j = 2; j < diem[0].length; j++) {
				System.out.println(tieuchi[i - 1] + "| " + trongso[i - 1] + "| " + diem[i - 1][j - 2]);
			}
		}
	}

	public void scoreCalculate() {
		int tmp;
		for (int j = 2; j < diem[0].length; j++) {
			System.out.println("Tinh diem theo trong so cua du an " + (j-1));
			for (int i = 1; i <= tieuchi.length; i++) {
				tmp = trongso[i-1]*diem[i-1][j-2];
				score=score+tmp;
			}
		}
		System.out.println(score/100);
	}

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		MainClass test = new MainClass();
		String filePath = System.getProperty("user.dir") + "\\data";
		test.readData(filePath, "BaitTapNo3.xlsx", "InputCau2");
		test.printData();
		test.scoreCalculate();
	}

}
