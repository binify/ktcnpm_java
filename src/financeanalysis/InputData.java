package financeanalysis;

public class InputData {
	private Double month;
	private Double revenue;
	private Double cost;
	
	public InputData(Double month, Double revenue, Double cost) {
		super();
		this.month = month;
		this.revenue = revenue;
		this.cost = cost;
	}
	
	public InputData() {
		// TODO Auto-generated constructor stub
		this.month = null;
		this.revenue = null;
		this.cost = null;
	}

	public Double getMonth() {
		return month;
	}
	public void setMonth(double d) {
		this.month = d;
	}
	public Double getRevenue() {
		return revenue;
	}
	public void setRevenue(Double revenue) {
		this.revenue = revenue;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
}
