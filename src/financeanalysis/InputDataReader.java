package financeanalysis;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class InputDataReader {

	public static List<InputData> listInput(String filePath, String fileName, String sheetName) throws IOException {
		// public void readExcel(String filePath, String fileName, String
		// sheetName) throws IOException {

		List<InputData> list = new ArrayList<InputData>();

		File file = new File(filePath + "\\" + fileName);
		FileInputStream inputStream = new FileInputStream(file);
		Workbook workBook = null;

		String fileExtension = fileName.substring(fileName.indexOf("."));

		if (fileExtension.equals(".xlsx")) {
			workBook = new XSSFWorkbook(inputStream);
		} else if (fileExtension.equals(".xls")) {
			workBook = new HSSFWorkbook(inputStream);
		}

		Sheet sheet = workBook.getSheet(sheetName);

		int lastRow = sheet.getLastRowNum();
		InputData input;
		for (int i = 2; i <= lastRow; i++) {
			Row row = sheet.getRow(i);
			input = new InputData();
			input.setMonth(row.getCell(0).getNumericCellValue());
			input.setRevenue(row.getCell(1).getNumericCellValue());
			input.setCost(row.getCell(2).getNumericCellValue());
			list.add(input);
		}
		
		return list;
	}
	public void printList(List<InputData> list){
		for(InputData temp: list){
			System.out.println(temp.getMonth());
		}
	}
	public static void main(String[] args) throws IOException {
		InputDataReader test = new InputDataReader();
		test.printList(listInput("E:\\Data", "BaitTapNo3.xlsx", "InputData"));
	}

}
