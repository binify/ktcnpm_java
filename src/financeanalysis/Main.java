package financeanalysis;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
	public static Double rate = 0.06;

	public void printDoubleList(List<Double> list) {
		for (int i = 0; i < list.size(); i++) {
			System.out.println("|" + (i + 1) + "|" + Math.round(list.get(i)));
		}
	}

	public Double sumList(List<Double> list) {
		Double sum = 0.0;
		for (Double temp : list) {
			sum = sum + temp;
		}
		return sum;
	}

	public static List<Double> cashFlowList(List<InputData> listInput) throws IOException {
		List<Double> list = new ArrayList<Double>();
		Double cashFlow;
		for (InputData temp : listInput) {
			cashFlow = temp.getRevenue() - temp.getCost();
			list.add(cashFlow);
		}
		return list;
	}

	public static List<Double> discountedCostList(List<InputData> listInp) throws IOException {
		List<Double> list = new ArrayList<Double>();
		Double discountedCost;
		for (int i = 0; i < listInp.size(); i++) {
			discountedCost = Math.floor(listInp.get(i).getCost() / Math.pow((1 + rate), i + 1));
			list.add(discountedCost);
		}
		return list;
	}

	public static List<Double> PVList(List<InputData> listInput) throws IOException {
		List<Double> listInp = cashFlowList(listInput);
		List<Double> list = new ArrayList<Double>();
		Double PV;
		for (int i = 0; i < listInp.size(); i++) {
			PV = listInp.get(i) / Math.pow((1 + rate), i + 1);
			list.add(PV);
		}
		return list;
	}

	public static List<Double> PBList(List<InputData> listInput) throws IOException {
		List<Double> listInp = PVList(listInput);
		List<Double> list = new ArrayList<Double>();
		Double sum = 0.0;
		for (Double temp : listInp) {
			sum = sum + temp;
			list.add(sum);
		}
		return list;
	}

	public static List<Double> FWList(List<InputData> listInput) throws IOException {
		List<Double> listInp = cashFlowList(listInput);
		List<Double> list = new ArrayList<Double>();
		Double FW = null;

		for (int i = 0; i < listInp.size(); i++) {
			FW = Math.floor(listInp.get(i) * Math.pow((1 + rate), (listInp.size() - i - 1)));
			list.add(FW);
		}
		return list;
	}

	public static double getIRR(List<Double> cashFlows) {
		final int MAX_ITER = 20;
		double EXCEL_EPSILON = 0.000001;

		double x = 0.1;
		int iter = 0;
		while (iter++ < MAX_ITER) {

			final double x1 = 1.0 + x;
			double fx = 0.0;
			double dfx = 0.0;
			for (int i = 0; i < cashFlows.size(); i++) {
				final double v = cashFlows.get(i);
				final double x1_i = Math.pow(x1, i);
				fx += v / x1_i;
				final double x1_i1 = x1_i * x1;
				dfx += -i * v / x1_i1;
			}
			final double new_x = x - fx / dfx;
			final double epsilon = Math.abs(new_x - x);

			if (epsilon <= EXCEL_EPSILON) {
				if (x == 0.0 && Math.abs(new_x) <= EXCEL_EPSILON) {
					return 0.0; // OpenOffice calc does this
				} else {
					return new_x * 100;
				}
			}
			x = Math.round(new_x * 100.0) / 100.0;
		}
		return x;
	}

	public static double paybackPeriod(List<InputData> listInp) throws IOException {
		Double costSum = 0.0;
		Double revenueSum = 0.0;
		for (int i = 0; i < listInp.size(); i++) {
			costSum = costSum + listInp.get(i).getCost();
			revenueSum = revenueSum + listInp.get(i).getRevenue();
		}
		Double avgRevenue = revenueSum / listInp.size();
		return Math.round((costSum / avgRevenue) * 100.0) / 100.0;
	}

	public static double ROCE(List<InputData> listInp) throws IOException {
		List<Double> list = cashFlowList(listInp);
		Double costSum = 0.0;
		for (int i = 0; i < listInp.size(); i++) {
			costSum = costSum + listInp.get(i).getCost();
		}
		Double sumCashFlow = 0.0;
		for (int i = 0; i < list.size(); i++) {
			sumCashFlow = sumCashFlow + list.get(i);
		}
		return Math.round((sumCashFlow / costSum) * 100.0) / 100.0;
	}

	public static double CRF(List<InputData> listInp) {
		double crf = (rate * Math.pow(1 + rate, listInp.size())) / (Math.pow(1 + rate, listInp.size()) - 1);
		return Math.round((crf) * 1000.0) / 1000.0;
	}

	public static double AF(List<InputData> listInp) throws IOException {
		double af = (1 - Math.pow(1 + rate, -listInp.size())) / rate;
		return Math.round((af) * 1000.0) / 1000.0;
	}

	public static void main(String[] args) throws IOException {
		String filePath = System.getProperty("user.dir") + "\\data";
		List<InputData> listInp = InputDataReader.listInput(filePath, "BaitTapNo3.xlsx", "InputCau1");
		Main test = new Main();

		// (1) Giá trị hiện thời
		System.out.println("Giá trị hiện thời: ");
		test.printDoubleList(cashFlowList(listInp));
		// (2) Giá trị tương lai;
		System.out.println("Giá trị tương lai: ");
		test.printDoubleList(FWList(listInp));
		// (3) Giá trị hiện tại ròng
		System.out.print("Giá trị hiện tại ròng: ");
		System.out.println(test.sumList(PVList(listInp)) + " USD");
		// (4) Giá trị tương lai ròng;
		System.out.print("Giá trị tương lai ròng: ");
		System.out.println(test.sumList(FWList(listInp)) + " USD");
		// (5) Hệ số hoàn trả vốn và Hệ số niên kim
		System.out.println("Hệ số hoàn trả vốn: " + Main.CRF(listInp));
		System.out.println("Hệ số niên kim: " + Main.AF(listInp));
		// (6) Giá trị tương đương hàng năm;
		System.out.println(
				"Giá trị tương đương hằng năm: " + Math.round((12 * test.sumList(PVList(listInp))) / Main.AF(listInp)));
		// (7) Tỷ lệ hoàn vốn nội bộ
		System.out.println("Tỉ lệ hoàn vốn nội bộ: " + Main.getIRR(cashFlowList(listInp)));
		// (8)Tỷ lệ thu hồi vốn nội bộ thấp nhất được chấp nhận
		System.out.println("Tỉ lệ thu hồi vốn được chấp nhận chọn trong khoảng [" + rate + ";"
				+ Main.getIRR(cashFlowList(listInp)) + "]");
		// (9) Cân bằng dự án;
		System.out.println("Cân bằng dự án: ");
		test.printDoubleList(PBList(listInp));
		// (10) Thời gian hoàn vốn;
		System.out.println("Thời gian hoàn vốn: " + Main.paybackPeriod(listInp) + " tháng");
		// (11) Chỉ số hoàn vốn đầu tư
		System.out.println("Chỉ số hoàn vốn đầu tư: "
				+ Math.round(test.sumList(PVList(listInp)) / test.sumList(discountedCostList(listInp)) * 100) + "%");
		// (12) Hệ số thu nhập trên vốn sử dụng
		System.out.println("Hệ số thu nhập trên vốn sử dụng: " + Main.ROCE(listInp));

	}
}
